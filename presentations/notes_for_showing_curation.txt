Plate 1. - 20190730S-023: 

Load CSV file with name, inchi, some local identifier, as well as other info such as CAS number for the compound. And then the path to the file where this compound was analyzed.

You can also go back to sets you have already loaded and I have done that here. You can see a file is selected.

Then below you get a list of all the files that were loaded.

You can click on a file. And you get the chromatogram. And you can see the information that was in the CSV as well as some calculated properties. And you get those for both the structure as it was entered and after removing potential salts.



You'd then click "process file" to find the peaks or ask it to pick peaks in all the files in one go.
What it does is find all the chromatographic peaks in the sample. Then use CAMERA to group the peaks that belong together.
And then only keep the pcgroups that contain an ion you would expect from the standard. That would be the pseudo-molecular ion, a sodium adduct, a water loss or I few others.

Down here you can set the settings for peak picking...


Here I have already done that and it has found who peaks that could match the injected standard.

clearly one peak is wrong --> look at why it was picked --> Deselect

Spectrum tab.


You can also add information here that would apply to all the files in your CSV.


Export --> so far only a Spectra object from the Spectra package.



The report