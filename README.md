# MScurate

* Input file needs to contain the columns:
  * compound_id
  * compound_name
  * mzML_path
  * InChI
  * CAS (can be blank)
  * inchi_key (can be blank)

* Finds peaks if they can be annotated. Requires an annotation with a quasi-molecular ion (currently [M+H]+, [M+NH4]+, [M+Na]+, [M+K]+, [M+H-H2O]+, [M-H]-, [M+Cl]-, [M-2H+Na]-, [M-H-H2O]-, [M-H+HCOOH]-).



