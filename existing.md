* **RMassBank**
  * Has all the machinery. Not very friendly.
  * https://github.com/MassBank/RMassBank
* **Curatr**
  * Full GUI for curation of spectra.
  * Doesn't seem to be very customizable.
  * Done in Python.
  * Actively maintained?
  * https://academic.oup.com/bioinformatics/article/34/8/1436/4741357#supplementary-data (screenshots in suppl.)
  * https://github.com/alexandrovteam/curatr
* **msPurity**
  * Might contain useful functions
  * https://github.com/computational-metabolomics/msPurity
* **masstrixR**
  * Seems to also have a lot of the functions to do it.
  * Not a lot of documentation/examples
  * https://github.com/michaelwitting/masstrixR
* **CompoundDb**
  * Use this as db

