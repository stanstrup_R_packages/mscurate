FROM rocker/shiny-verse:4.0.3


##### System libs setup #######################################################
# Install needed linux tools
RUN apt-get update && \
    apt-get install  -y \
	# can't compile this
	r-cran-igraph \
	# needed for certain R packages
	libnetcdf-dev \
	libxml2-dev \
	libssl-dev \
	\
	default-jre \
	default-jdk \
	liblzma-dev \
	libbz2-dev \
	\
	librsvg2-dev \
	\
    && rm -rf /var/lib/apt

RUN sudo R CMD javareconf
###############################################################################


##### Install R packages ######################################################
RUN R -e "Sys.setenv(R_REMOTES_NO_ERRORS_FROM_WARNINGS=TRUE);BiocManager::install(ask=FALSE)"
RUN R -e "Sys.setenv(R_REMOTES_NO_ERRORS_FROM_WARNINGS=TRUE);install.packages('BiocManager');BiocManager::install('remotes');BiocManager::install('devtools')"

RUN mkdir "/srv/shiny-server/shiny_mscurate"
RUN mkdir "/srv/shiny-server/shiny_mscurate/shiny_mscurate"
COPY "./install_packages.R" /srv/shiny-server/shiny_mscurate/shiny_mscurate/

RUN Rscript "/srv/shiny-server/shiny_mscurate/shiny_mscurate/install_packages.R"

COPY ./ /srv/shiny-server/shiny_mscurate/
###############################################################################


##### Setup shared host folders ###############################################
RUN mkdir /data
RUN mkdir -p /media/hostsource

RUN chown -R shiny:shiny /data
RUN chmod -R 755 /data
###############################################################################


##### File permissions ########################################################
# make all app files readable (solves issue when dev in Windows, but building in Ubuntu)
# Evidently not needed and takes a lot of space
# RUN chown -R shiny:shiny /srv/shiny-server
# RUN chmod -R 755 /srv/shiny-server/
###############################################################################

##### Shiny config ############################################################
COPY ./shiny-server.conf /etc/shiny-server/shiny-server.conf
EXPOSE 3838
###############################################################################