# Object to keep all data -------------------------------------------------
results_static <- tibble(file_idx =             integer(),
                         filepath =             character(),
                         filename =             character(),
                         filename_sansext =     character(), 
                         func =                 character(),
                         msLevel =              integer(),
                         polarity =             integer(),
                         OnDiskMSnExp =         list(),
                         
                         collisionEnergy =              numeric(), 
                         precursorMZ =                  numeric(),  
                         isolationWindowTargetMZ =      numeric(),  
                         isolationWindowLowerOffset =   numeric(),  
                         isolationWindowUpperOffset =   numeric(),  
                         scanWindowLowerLimit =         numeric(),  
                         scanWindowUpperLimit =         numeric(), 
                         
                         # peak picking
                         XCMSnExp =             list(),
                         
                         # spectra =              list(),
                         spectra_filtered =     list(),
                         
                         TIC =                  list(),
                         BPI =                  list(),
                         
                         decision =             character()

                         )


results <- reactiveVal(results_static)




# Asynchronous reading of files -------------------------------------------  
# parallel code based on https://stackoverflow.com/questions/56267073/how-can-i-make-ui-respond-to-reactive-values-in-for-loop/56271695
# Create queue
queue <- shinyQueue()
queue$consumer$start(100)

# initial partial object
OnDiskMSnExp_list <- reactiveVal()


# read file and save to OnDiskMSnExp_list
observeEvent(input$run,{
  
  file_paths <- input_tab() %>% filter(mzML_path %in% valid_paths()) %>% pull(mzML_path)

  # if nothing ever initialized it is empty
  if(is.null(results()$filepath)){ 
    already_done <- rep(FALSE, length(file_paths))
  }else{
    already_done <- normalizePath(file_paths) %in% normalizePath(results()$filepath)    
  }
  

  raws <- list()
  
  future({
    for(i in seq_along(file_paths)[!already_done]){
      raws[[i]] <- readMSData(file_paths[i], mode = "onDisk")
      queue$producer$fireAssignReactive("OnDiskMSnExp_list", raws)
      
    }
  })

  # disable initialize button when clicked
  disable("run")
  
  
  NULL

})



# Create table with basic info, raw data and processed data ---------------
observeEvent(OnDiskMSnExp_list(), {

  req(OnDiskMSnExp_list())

  
  
    # if nothing ever initialized it is empty
  if(is.null(results()$filepath)){ 
    already_done <- rep(FALSE, length(compact(OnDiskMSnExp_list())))
  }else{
    already_done <- map_chr(compact(OnDiskMSnExp_list()), fileNames) %in% results()$filepath
  }

  
  
  
  # Get simple data about the file and add the raw data
  result <- OnDiskMSnExp_list() %>% 
            compact() %>% 
            extract(!already_done) %>% 
            {tibble(filepath = map_chr(., fileNames),
                    filename = basename(filepath),
                    filename_sansext = file_path_sans_ext(filename), 
                    OnDiskMSnExp = .
                   )
            }
  
  # add spectra summary
  result <- result %>% 
              mutate(fData = map(OnDiskMSnExp, ~ ..1 %>%
                                   fData %>% 
                                   mutate(func = gsub("function=(.*) process=(.*) scan=(.*)","\\1",spectrumId)) %>% 
                                   select(func, msLevel, polarity, collisionEnergy, precursorMZ, contains("Window")) %>% 
                                   distinct() %>% 
                                   as_tibble()
                                )
                     ) %>% 
              unnest(fData)
  
  
  # subset OnDiskMSnExp to the selected func, msLevel, polarity
  result <- result %>% 
            mutate(func_idx = map2(OnDiskMSnExp, func, find_func)) %>% 
            mutate(OnDiskMSnExp = pmap(list(OnDiskMSnExp, msLevel, polarity, func_idx), 
                                       ~..1 %>% 
                                         filterMsLevel(..2) %>% 
                                         filterPolarity(..3) %>% 
                                         filterAcquisitionNum(..4)
                                       )
                   ) %>% 
            select(-func_idx)
  
  
  # Set decision undecided
  result$decision <- "Undecided"
  
  
  
  
  
  
  
  
  # update the complete set of data.
  if(is.null(results())){
  
      results(
          rows_upsert(results_static,
                      result,
                      by = c("filepath", "func", "msLevel", "polarity")
                      ) %>% 
          mutate(file_idx = factor(filepath, unique(filepath))) %>% 
          mutate(file_idx = as.integer(file_idx))
          )
    
  }else{
    
      results(
          rows_upsert(results(),
                      result,
                      by = c("filepath", "func", "msLevel", "polarity")
                      ) %>% 
          mutate(file_idx = factor(filepath, unique(filepath))) %>% 
          mutate(file_idx = as.integer(file_idx))
          )
    
  }
  

  
  
  # Write to cache
  mscurate_update_cache(digest(input_tab() %>% select(compound_id, compound_name, mzML_path, inchi_input), algo="md5"), what = c("result", "Sys_time"))
 
    
  
  # signal we are done so the input table can update.
  triggers$file_init <- triggers$file_init + 1
  
  
})


observeEvent(results(),{ # results changed. New processing done.
  # make sure all files have local settings
  
  update_processing_settings()
  
  settings$processing_local <- results() %>% 
                                  select(file_idx, func) %>% 
                                  mutate(settings = list(settings$processing_global)) %>% 
                                  anti_join(., settings$processing_local, by = c("file_idx", "func")) %>% 
                                  bind_rows(settings$processing_local)
   
  
   
   if(is.null(settings$local)){
   settings$local <- results() %>% 
                                  slice(0) %>% 
                                  select(file_idx,filepath, func, msLevel, polarity) %>% 
                                  mutate(enabled = FALSE, set_id = character())
   }
   
   
   # Makes sure all files have enabled/disabled state for all ids
   settings$local <- results() %>% 
                                  select(file_idx,filepath, func, msLevel, polarity) %>% 
                                  mutate(enabled = FALSE, set_id = list(ids)) %>% 
                                  anti_join(., settings$local, by = c("file_idx", "func")) %>% 
                                  unnest(set_id) %>% 
                                  bind_rows(settings$local)
   

   #Make sure all initialized files have local/run info.
    settings$info_local <- results() %>% 
                                  select(file_idx,func) %>% 
                                  anti_join(., settings$info_local, by = c("file_idx", "func")) %>% 
                                  mutate(settings = rep(list(as.list(setNames(c("Commercial standard", ""), c("anal_info_run_compound_source", "anal_info_run_comment")))) , nrow(.))) %>% 
                                  bind_rows(settings$info_local)
   

})
