update_spectra_with_info <- function(){
  
  progress <- Progress$new()
  on.exit(progress$close())
  progress$set(message = "Updating results with user info", value = 0)
    
    
  new_spectra_filtered <- results()$spectra_filtered
  
      
  # update global info
  if(!is.null(results()) && nrow(results())>0 && length(compact(results()$spectra_filtered))>0){ # if any processed at all add global info to all
        
    
      
    info_global <- settings$info_global %>%
                      DataFrame() %>%
                      setNames(., gsub("anal_info_batch_", "",names(.))) 
      
      
    
    for(i in seq_along(new_spectra_filtered)){

        if(class(new_spectra_filtered[[i]])  != "Spectra") next # no processing done for this file
      
      
        rep_idx <- names(new_spectra_filtered[[i]]@backend@spectraData) %in% names(info_global)
        
        
        new_spectra_filtered[[i]]@backend@spectraData   <- new_spectra_filtered[[i]]@backend@spectraData[,!rep_idx] %>%
                                                                cbind(info_global, .)

      }

    
    
  
  # update local/run settings
    if(nrow(settings$info_local)>0){
      
      for(i in 1:nrow(settings$info_local)){
        
        
        result_idx <- which(results()$file_idx == settings$info_local$file_idx[i] & results()$func == settings$info_local$func[i])
        
        if(class(new_spectra_filtered[[result_idx]])  != "Spectra") next
    	
        # inplace replacement is the fastest
        new_spectra_filtered[[result_idx]]@backend@spectraData$comment <-         settings$info_local$settings[[i]]$anal_info_run_comment
        
        new_spectra_filtered[[result_idx]]@backend@spectraData$compound_source <- settings$info_local$settings[[i]]$anal_info_run_compound_source
        
      }
    }


  
  
    # update reactive
    results() %>% 
      mutate(spectra_filtered = new_spectra_filtered) %>% 
      results()
    
    mscurate_update_cache(digest(input_tab() %>% select(compound_id, compound_name, mzML_path, inchi_input), algo="md5"), c("result"))
  
    
  
  }
  
  
}
  


