library(dplyr)
library(tidyr)
library(magrittr)
library(webchem)
library(purrr)
library(CompoundDb)
library(DBI)
library(rcdk)
library(rinchi)



source("FUNS.R")

stds <- readRDS("stds.rds")


# stds <- stds[c(1:5, 387),]
# stds$CAS[1] <- NA




# Make compoundDB ---------------------------------------------------------
stds_db_file <- stds %>% 
            dplyr::rename(compound_name = better_name_for_database, inchi = InChI) %>% 
            mutate(compound_id = 1:n()) %>% 
            mutate(formula = as.character(NA), mass = as.numeric(NA), inchi_key = as.character(NA), synonyms = as.character(NA)) %>% 
                  createCompDb(
                         metadata = make_metadata(source = "Ldra_lab", url=NA, source_version = "v3", source_date = Sys.Date(), organism = "STD")
                        )

stds_db <- CompDb(stds_db_file, flags = RSQLite::SQLITE_RW)




# Get InChI from cts_convert ----------------------------------------------
c("ALTER TABLE synonym ADD source TEXT;",
  "ALTER TABLE synonym ADD source_idx INTEGER;") %>% 
  map(~dbExecute(dbconn(stds_db), .x))


"
CREATE TABLE 'CTS' (
	'compound_id'	INTEGER,
	'SMILES'	TEXT,
	'inchicode'	TEXT,
	'inchikey'	TEXT,
	'formula'	TEXT,
	'source_idx' INTEGER
);
" %>% 
  map(~dbExecute(dbconn(stds_db), .x))



for(i in unique(compounds(stds_db)$compound_id)){
  
  # make it possible to continue where you left off
  existing_cts <- "SELECT DISTINCT compound_id FROM CTS;" %>% 
    dbGetQuery(dbconn(stds_db), .) %>% 
    pull(compound_id)
  
  
  if(i %in% existing_cts) next
  
  
  cts <- stds_db %>% 
            compounds %>% 
            as_tibble %>% 
            filter(compound_id==i) %>% 
            pull(CAS) %>% 
            cts_cas_fun()
            
  
  # next if no results
  if(length(cts)==1 && is.na(cts)) next
  
  
  # write CTS info to seperate table
  cts %>% 
    map_dfr(~.x %>% extract(c("inchikey", "inchicode", "formula")) %>% as_tibble) %>% 
    mutate(source_idx=1:n()) %>% 
    mutate(compound_id=i) %>% 
    mutate(SMILES = map(inchicode, ~ parse.inchi_do(.x))) %>%
    mutate(SMILES = map_chr(SMILES, ~ if(is.null(.x[[1]])){""}else{get.smiles_p(.x[[1]], smiles.flavors(flavors = c("Isomeric")))})) %>% 
    dbWriteTable(dbconn(stds_db), "CTS", ., append = TRUE, row.names = FALSE)
  
  
  # synonyms
  cts %>% 
    map_dfr(cts_extract) %>% 
    mutate(compound_id=i) %>% 
    select(compound_id, synonyms) %>% 
    mutate(source= "CTS",source_idx=1:n()) %>% 
    unnest(synonyms) %>% 
    dplyr::rename(synonym=name) %>% 
    distinct() %>% 
    dbWriteTable(dbconn(stds_db), "synonym", ., append = TRUE, row.names = FALSE)
  
  
  # external refs
  cts %>% 
    map_dfr(cts_extract) %>% 
    mutate(compound_id=as.integer(i)) %>% 
    select(compound_id, externalIds) %>% 
    mutate(source= "CTS",source_idx=1:n()) %>% 
    unnest(externalIds) %>% 
    distinct() %>% 
    filter(name %in% c("ChEBI", "CAS", "NIST", "ChemSpider", "PubChem CID", "Human Metabolome Database")) %>% 
    dplyr::rename(ex_name = name, ex_id = value, ex_url = url) %>% 
    dbWriteTable(dbconn(stds_db), "links", ., append = TRUE, row.names = FALSE)
  
  
}

# reconnect to get new data
dbDisconnect(dbconn(stds_db))
stds_db <- CompDb(stds_db_file, flags = RSQLite::SQLITE_RW)




# PubChem -----------------------------------------------------------------
"
CREATE TABLE 'PubChem' (
	'compound_id'	INTEGER,
	'source_idx' INTEGER,
	'CID'	TEXT,
	'CanonicalSMILES'	TEXT,
	'IsomericSMILES'	TEXT,
	'InChI'	TEXT,
	'InChIKey'	TEXT,
	'IUPACName'	TEXT,
	'ExactMass'	REAL,
	'MonoisotopicMass' REAL,
	'Charge' INTEGER
	);
" %>% 
  map(~dbExecute(dbconn(stds_db), .x))







for(i in unique(compounds(stds_db)$compound_id)){
  
  
  # make it possible to continue where you left off
  existing_cts <- "SELECT DISTINCT compound_id FROM PubChem;" %>% 
    dbGetQuery(dbconn(stds_db), .) %>% 
    pull(compound_id)
  
   if(i %in% existing_cts) next
  
  
  
  
  
  
  pc <- stds_db %>% 
            compounds %>% 
            as_tibble %>% 
            filter(compound_id==i) %>% 
            pull(CAS) %>% 
            pc_from_cas()
            
  
  if(nrow(pc)==0) next

  # Remove not found
  pc <- pc %>% filter(!is.na(CID))
  
  
    # write PubChem info to seperate table
  pc %>% 
    mutate(source_idx=1:n()) %>% 
    mutate(compound_id=i) %>% 
    select( c("compound_id", "source_idx", "CID", "CanonicalSMILES", "IsomericSMILES", "InChI", "InChIKey", "IUPACName", "ExactMass", "MonoisotopicMass", "Charge" )) %>% 
    dbWriteTable(dbconn(stds_db), "PubChem", ., append = TRUE, row.names = FALSE)
  
  # synonyms
  pc %>% 
    mutate(compound_id=i) %>% 
    select(compound_id, synonyms) %>% 
    mutate(source= "PubChem",source_idx=1:n()) %>% 
    unnest(synonyms) %>% 
    dplyr::rename(synonym=synonyms) %>% 
    distinct() %>% 
    dbWriteTable(dbconn(stds_db), "synonym", ., append = TRUE, row.names = FALSE)
  
  
  
}

# reconnect to get new data
dbDisconnect(dbconn(stds_db))
stds_db <- CompDb(stds_db_file, flags = RSQLite::SQLITE_RW)



