# TODO
# Filter on something else than CAS. Not all structures have CAS numbers. Mixes etc. Maybe make a seperate set
# Some CAS are NA. Some are empty strings. Change all to NA
# Suggest recording the vendor in the future
# Also source. As in synthesized

# Libraries ---------------------------------------------------------------
library(dplyr)
library(purrr)
library(readxl)
library(janitor)
library(tidyr)
source("FUNS.R")

get_desc_from_raw_p <- possibly(get_desc_from_raw,tibble(desc = NA, Position = NA))



# Get info that can be read from the raw files ----------------------------
# find .raw files
files <- "I:/SCIENCE-NEXS-NyMetabolomics/Projects/Standards/New_std_link" %>% 
            fs::path_real() %>% 
            as.character() %>% 
            list.files("\\.raw$", full.names = TRUE, include.dirs = TRUE, recursive = TRUE)


# read .raw description
files_tab <- tibble(path = files) %>% 
               mutate(Desc = map(files, get_desc_from_raw_p)) %>% 
               unnest(Desc)


# Seperate info from description
files_tab <- files_tab %>% 
                filter(grepl("CAS",Desc)) %>% 
                mutate_if(is.character, ~gsub("\\?","",.x)) %>%  # strange "?"
                separate(Desc, c(NA, "CAS", "name_desc"), sep="_", remove = FALSE) %>% 
                mutate(machine = ifelse(grepl("Synapt", path),"Synapt",NA)) %>% 
                mutate(machine = ifelse(grepl("Qtof", path),"Qtof",machine)) %>% 
                separate(Position, c("Plate_pos", "Position"), sep=":", convert = TRUE) %>% 
                mutate(Position = gsub(",","",Position)) %>% 
                mutate(Job = as.integer(gsub(".*([0-9]+).*","\\1",Job))) %>% 
                dplyr::rename(Plate = Job) %>% 
                mutate_if(is.character, trimws) %>%  # whitespace
                mutate_if(is.character, ~ifelse("NA"==.x,NA,.x))
   



# Read list of standards --------------------------------------------------
# read file
stds <- "I:/SCIENCE-NEXS-NyMetabolomics/Projects/Standards/New_std_link/Standard library list.xlsx" %>% 
            fs::path_real() %>% 
            as.character() %>% 
            map2(., paste0("Plade ",1:9), ~ read_excel(path = ..1, sheet = ..2))
            
  
stds <- stds %>%  
          map(clean_names) %>% 
          map(~ mutate_all(.x, ~ trimws(as.character(.x))) ) %>% 
          bind_rows(.id = "Plade")

  
# clean up columns
stds <- stds %>% 
         select(-contains("cas"), CAS = cas_no_2) %>% 
         select(-contains("better"), better_name_for_database = better_name_for_database_4) %>% 
         dplyr::rename(InChI = in_ch_i) %>% 
         select(-starts_with("x")) %>% 
         rename(Plate = plate, Position = position)


# set reasonable classes
stds <- stds %>% 
          mutate_at(vars(Plade, Plate), as.integer) %>% 
          mutate_at(vars(dissolve_in), as.factor) %>% 
          mutate_at(vars(weigh_x_mg), as.numeric)


# making real NAs
stds <- stds %>% 
          mutate_at(vars(CAS, chemical_name, no, better_name_for_database, InChI), ~ifelse("NA"==.x,NA,.x))

# Remove control character
stds <- stds %>% 
          mutate(CAS = gsub("\U200E", "", CAS))





# Merge filelist and std list ---------------------------------------------
stds_files <- files_tab %>% 
                full_join(stds, by = c("Plate", "Position"))


# check that CAS are identical:
#stds_files %>% filter(CAS.x != CAS.y) 

# One CAS to rule them all
stds_files <- stds_files %>% select(everything(), CAS = CAS.x, -CAS.y)




saveRDS(stds_files, "stds_files.rds")

